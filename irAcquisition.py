"""Execute this script in a Python 3.6 environnement."""

import subprocess

import cv2
import lynred_py
import numpy as np

print(cv2.__version__)


def get_screen_resolution():
    """
    This function retrieves the screen resolution of the primary monitor.

    Parameters:
    None

    Returns:
    tuple: A tuple containing the screen width and height in pixels.

    Raises:
    None

    Note:
    This function uses the 'xrandr' command-line tool to retrieve the screen resolution.
    It assumes that 'xrandr' is installed and available in the system's PATH.
    """
    output = subprocess.Popen(
        'xrandr | grep "\*" | cut -d" " -f4', shell=True, stdout=subprocess.PIPE
    ).communicate()[0]
    resolution = output.split()[0].split(b"x")
    return int(resolution[0]), int(resolution[1])


def take_snapshot(camera, close):
    """
    This function acquires the infrared image from the camera
    and displays it in a cv2 window.
    It also allows the user to save the image by pressing the 's' key.

    Parameters:
    camera (lynred_py.acq.camera_ati320): The camera object used to
    capture the infrared image.
    close (bool): A flag indicating whether to close the camera after
    the acquirement.

    Returns:
    None

    Raises:
    lynred_py.error.error_t: If an error occurs while getting the image.

    Note:
    This function uses the OpenCV library to display and handle the cv2 window.
    """
    
    camera.start() # Start the camera
    resolution = get_screen_resolution()  # Get monitor resolution
    cv2.namedWindow("Infrared Image", cv2.WINDOW_NORMAL)  # Name the window
    cv2.moveWindow(
        "Infrared Image", resolution[0], resolution[1]
    )  # Move window on the right-bottom corner
    
    while True:
        image = lynred_py.base_t.image_t()
        camera.get_image(image)  # Get image from camera
        if image.empty():
            raise lynred_py.error.error_t("Error while getting image")

        img = np.array(image)  # Convert to numpy array

        # Display ir image in a cv2 window
        cv2.imshow("Infrared Image", img)

        # If 'q' button is pressed, quit cv2 window and finish script execution
        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"):
            break
        # If 's' button is pressed, save the image in the 'images' folder
        elif key == ord("s"):
            image.save("images/ir.png")
            print("image saved")

    # Clean window and stop camera
    cv2.destroyAllWindows()
    camera.stop()

    if close:
        camera.close()


if __name__ == "__main__":
    try:
        # Discover UVC cameras connected to the system
        uvc_cameras = lynred_py.acq.discover_cameras(
            lynred_py.acq.camera_discover_type_t.camera_discover_type_uvc
        )
        # Print the name of the first discovered camera
        print("Connecting to the camera " + uvc_cameras[0].c_str())
        # Create a camera object for the first discovered UVC camera
        camera = lynred_py.acq.create_camera_ati320(
            lynred_py.acq.video_grabber_type_t.video_grabber_type_uvc,
            uvc_cameras[0].c_str(),
        )
        # Open the camera with specific serial port,
        # communication protocol, and writer type
        camera.open(
            "/dev/serial/by-id/usb-FullScale_FullScale_USB3-A_v3.006-if02",
            lynred_py.communication.communication_protocol_type_t.communication_protocol_type_fullscale,
            lynred_py.communication.communication_writer_type_t.communication_writer_type_serial,
        )

        # Set camera registers
        camera.set_register(513, 1)
        # print(camera.get_register(2561))
        camera.set_register(516, 1)

        # Take a snapshot of the infrared image from the camera
        # and display it in a cv2 window
        take_snapshot(camera, True)

    except lynred_py.error_t as e:
        print("lynred_py.error_t caught: {}".format(e))
    except RuntimeError as e:
        print("RuntimeError caught: {}".format(e))
    except Exception as e:
        print("exception caught: {}".format(e))
