import os
import time

import cv2
import mediapipe as mp

from utils import (
    isPosition1,
    isPosition2,
    isPosition3,
    isPosition4,
    isPosition5,
    isPosition6,
    isPosition7,
    isPosition8,
    isPosition9,
)


def init_positions() -> tuple:
    """
    Initialize the list of position checking functions and the corresponding image paths

    The function reads the selected positions from a text file
    named "selected_positions.txt".
    Each line in the file contains the path to an image representing a specific position
    The function extracts the position number from the image file name and uses it
    to index into the list of position checking functions.

    Returns:
    final_positions (list): A list of position checking functions.
    images_path (list): A list of image paths representing the selected positions.
    """
    positions = [
        isPosition1,
        isPosition2,
        isPosition3,
        isPosition4,
        isPosition5,
        isPosition6,
        isPosition7,
        isPosition8,
        isPosition9,
    ]
    final_positions = []
    images_path = []
    with open("selected_positions.txt", "r") as file:
        lines = file.readlines()  # Read the lines from the file
        # print(lines)
        for line in lines:
            line = line.split("\n")[0]
            images_path.append(line)
            line = line.split("/")[-1].split(".")[0][4:]
            # Append the position checking function to the list
            final_positions.append(positions[int(line) - 1])
    return final_positions, images_path


def check_save(img_path: str, last_save: float) -> bool:
    """
    Check if the image file has been modified since the last save.

    This function checks if the image file at the given path has been modified
    since the last save by comparing the modification time of the file with the
    provided last_save timestamp.

    Parameters:
    img_path (str): The path to the image file to check.
    last_save (float): The timestamp of the last save.
    If None, the function will return True.

    Returns:
    bool: True if the image file has been modified since the last save, False otherwise.
    """
    if os.path.exists("images/ir.png") is False:
        return False
    if os.path.getmtime(img_path) == last_save or last_save is None:
        return False
    return True


def check_sequence(positions: list, landmarks: list, current_position: int) -> str:
    """
    Check the current position against the landmarks and return the next step.

    This function checks if the landmarks correspond to the current position.
    If the landmarks match, it returns the next position. If the landmarks do not match,
    it returns an error message. If the sequence is complete,
    it returns a finish message.

    Parameters:
    positions (list): A list of functions that check if the landmarks correspond to a
    specific position.
    landmarks (list): A list of landmarks detected by the pose estimation model.
    current_position (int): The current position number.

    Returns:
    str: The next step ("next", "error", or "finish").
    """
    if positions[current_position](landmarks):
        next_position = (current_position + 1) % len(positions)
        if next_position == 0:
            print("Séquence complète ! Un indice est débloqué.")
            return "finish"
        else:
            print(
                f"Position {current_position + 1} correcte. Pose {next_position + 1}."
            )
        return "next"
    else:
        print(f"Position {current_position + 1} incorrecte. Retour à la position 1.")
        return "error"


# Get the list of position checking functions and the corresponding image paths
positions, images_path = init_positions()
# Initiate the pose estimation model
mp_pose = mp.solutions.pose
pose = mp_pose.Pose(static_image_mode=True, min_detection_confidence=0.5)

mp_drawing = mp.solutions.drawing_utils

# If file exists : else last_save = None
if os.path.exists("images/ir.png"):
    last_save = os.path.getmtime("images/ir.png")
else:
    last_save = None

# Initiate the variables
new_pose = False
current_position = 0
start_time = time.time()
step = "next"
cv2.namedWindow("Pose estimation with MediaPipe", cv2.WINDOW_NORMAL)
# Window in fullscreen
cv2.setWindowProperty(
    "Pose estimation with MediaPipe", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN
)

while True:
    try:
        # Current position
        pose_path = images_path[current_position]
        # Plot the good image
        if step == "next":
            image_pose = cv2.imread(pose_path)
        elif step == "error":
            image_pose = cv2.imread("images/erreur.png")
        else:
            image_pose = cv2.imread("images/indice.png")

        # Check if the image file has been modified since the last save
        new_pose = check_save("images/ir.png", last_save)

        # Process the image
        if new_pose is True:
            image_ir = cv2.imread("images/ir.png")
            results = pose.process(image_ir)
            start_time = time.time()
        else:
            results = pose.process(image_pose)

        # If position is detected
        if results.pose_landmarks is not None and step != "finish" and new_pose is True:
            """if step == "next":
                mp_drawing.draw_landmarks(
                    image, results.pose_landmarks, mp_pose.POSE_CONNECTIONS
                )"""
            landmarks = results.pose_landmarks.landmark
            step = check_sequence(positions, landmarks, current_position)

        # If step is next
        if step == "next":
            text = f"REPRODUISEZ LA POSITION {current_position+1}"
            position = (520, 70)
            font = cv2.FONT_HERSHEY_DUPLEX
            scale = 2
            color = (40, 40, 40)
            bold = 3
            cv2.putText(
                image_pose, text, position, font, scale, color, bold, cv2.LINE_AA
            )

        # Plot image
        cv2.imshow("Pose estimation with MediaPipe", image_pose)

        # If a new pose is registered then last_save is updated
        if new_pose is True:
            last_save = os.path.getmtime("images/ir.png")
        # If step is next update current_position
        if step == "next":
            if new_pose is True:
                new_pose = False
                start_time = time.time()
                current_position = (current_position + 1) % 9
                time.sleep(0.01)
        # If step is error wait 10 seconds
        elif step == "error":
            elapsed_time = time.time() - start_time
            if elapsed_time >= 10:
                start_time = time.time()
                current_position = 0
                step = "next"
                time.sleep(0.01)
        # If step is finish wait 100 seconds and quit the program
        else:
            elapsed_time = time.time() - start_time
            if elapsed_time >= 100:
                break
    except Exception:
        pass

    # If the 'q' key is pressed, break from the loop
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break

cv2.destroyAllWindows()
