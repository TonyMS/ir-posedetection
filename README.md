# Infrared Pose Detection

This repository is the base for _Infrared Pose Detection_ demonstration.
The software requires a _Lynred_ camera.

## Step 1: Start the environment

First, to start the environment you will need to run the `devcontainer` with _VS Code_.
(More info [here](https://code.visualstudio.com/docs/devcontainers/containers))

## Step 2: Select the positions

When the environment is correctly setup, you can start the **GUI** to select the positions that are going to be shown to the user during the demonstration.

```bash
python poseSelection.py
```

At the end of the **GUI**, the selected positions will be saved in the file `selected_positions.txt`.

## Step 3: Run the acquisition of the infrared camera

After that, the demonstration can be started. 
First you will need to run the following command with **python3.6** to start the acquisition of the infrared images.

```bash
python3.6 irAcquisition.py
```

This script opens a window where the infrared images are displayed. 
The user can press the button <kbd>s</kbd> on the keyboard to save the image at the path `images/ir.png`.


## Step 4: Run the pose detection

Finally, in a different terminal, start the pose detection script by running the following command :

```bash
python poseDetection.py
```

A window is created and show a body position. A first user has to reproduce the position.
A second user can press the button <kbd>s</kbd> on the keyboard to validate the position.
The script read the infrared image saved at the path `images/ir.png` and predict the pose.
If the pose made by the first user is the same as the one displayed on the screen, a new position is displayed. Else, an error is printed and the user has to restart all the previous positions.
When all the poses are successfully made by the user, a hint is displayed at the screen and the demonstration ends.

> **Note**: The infrared acquisition script and the pose detection script must be run in parallel (for example in two different terminal processes). When the second user wants to validate the position of the first user, he must press <kbd>s</kbd> by being on the infrared acquisition window and not the position window.