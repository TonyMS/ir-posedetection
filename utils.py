import mediapipe as mp
import numpy as np

mp_pose = mp.solutions.pose


def calculate_angle(a: list, b: list, c: list) -> float:
    """
    Calculate the angle formed by three keypoints in a 2D space.

    Parameters:
    a (list): Coordinates of the first keypoint in the format [x, y].
    b (list): Coordinates of the second keypoint in the format [x, y].
    c (list): Coordinates of the third keypoint in the format [x, y].

    Returns:
    float: The angle in degrees between the line connecting keypoints a and b,
    and the line connecting keypoints b and c.
           Returns -1 if any of the keypoints have coordinates outside the range [0, 1].

    Note:
    This function assumes that the input keypoints are in a 2D space.
    """
    a = np.array(a)  # Keypoint 1
    b = np.array(b)  # Keypoint 2
    c = np.array(c)  # Keypoint 3

    keypoints = np.array([a, b, c])

    if np.any(keypoints < 0) or np.any(keypoints > 1):
        return -1

    radians = np.arctan2(c[1] - b[1], c[0] - b[0]) - np.arctan2(
        a[1] - b[1], a[0] - b[0]
    )
    angle = np.abs(radians * 180.0 / np.pi)

    if angle > 180.0:
        angle = 360 - angle

    return angle


def angleLeftArm(landmarks):
    """
    Calculate the angle between the left shoulder, elbow and wrist.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    float: The angle in degrees between the left shoulder, elbow and wrist.

    Note:
    This function assumes that the input landmarks are in a 2D space.
    """
    left_shoulder = [
        landmarks[mp_pose.PoseLandmark.LEFT_SHOULDER.value].x,
        landmarks[mp_pose.PoseLandmark.LEFT_SHOULDER.value].y,
    ]
    left_elbow = [
        landmarks[mp_pose.PoseLandmark.LEFT_ELBOW.value].x,
        landmarks[mp_pose.PoseLandmark.LEFT_ELBOW.value].y,
    ]
    left_wrist = [
        landmarks[mp_pose.PoseLandmark.LEFT_WRIST.value].x,
        landmarks[mp_pose.PoseLandmark.LEFT_WRIST.value].y,
    ]

    return calculate_angle(left_shoulder, left_elbow, left_wrist)


def angleRightArm(landmarks):
    """
    Calculate the angle between the right shoulder, elbow and wrist.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    float: The angle in degrees between the right shoulder, elbow and wrist.

    Note:
    This function assumes that the input landmarks are in a 2D space.
    """
    right_shoulder = [
        landmarks[mp_pose.PoseLandmark.RIGHT_SHOULDER.value].x,
        landmarks[mp_pose.PoseLandmark.RIGHT_SHOULDER.value].y,
    ]
    right_elbow = [
        landmarks[mp_pose.PoseLandmark.RIGHT_ELBOW.value].x,
        landmarks[mp_pose.PoseLandmark.RIGHT_ELBOW.value].y,
    ]
    right_wrist = [
        landmarks[mp_pose.PoseLandmark.RIGHT_WRIST.value].x,
        landmarks[mp_pose.PoseLandmark.RIGHT_WRIST.value].y,
    ]

    return calculate_angle(right_shoulder, right_elbow, right_wrist)


def angleLeftBodyArm(landmarks):
    """
    Calculate the angle between the left hip, shoulder, and elbow.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    float: The angle in degrees between the left hip, shoulder, and elbow.

    Note:
    This function assumes that the input landmarks are in a 2D space.
    """
    left_hip = [
        landmarks[mp_pose.PoseLandmark.LEFT_HIP.value].x,
        landmarks[mp_pose.PoseLandmark.LEFT_HIP.value].y,
    ]
    left_shoulder = [
        landmarks[mp_pose.PoseLandmark.LEFT_SHOULDER.value].x,
        landmarks[mp_pose.PoseLandmark.LEFT_SHOULDER.value].y,
    ]
    left_elbow = [
        landmarks[mp_pose.PoseLandmark.LEFT_ELBOW.value].x,
        landmarks[mp_pose.PoseLandmark.LEFT_ELBOW.value].y,
    ]

    return calculate_angle(left_hip, left_shoulder, left_elbow)


def angleRightBodyArm(landmarks):
    """
    Calculate the angle between the right hip, shoulder, and elbow.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    float: The angle in degrees between the right hip, shoulder, and elbow.

    Note:
    This function assumes that the input landmarks are in a 2D space.
    """
    right_hip = [
        landmarks[mp_pose.PoseLandmark.RIGHT_HIP.value].x,
        landmarks[mp_pose.PoseLandmark.RIGHT_HIP.value].y,
    ]
    right_shoulder = [
        landmarks[mp_pose.PoseLandmark.RIGHT_SHOULDER.value].x,
        landmarks[mp_pose.PoseLandmark.RIGHT_SHOULDER.value].y,
    ]
    right_elbow = [
        landmarks[mp_pose.PoseLandmark.RIGHT_ELBOW.value].x,
        landmarks[mp_pose.PoseLandmark.RIGHT_ELBOW.value].y,
    ]

    return calculate_angle(right_hip, right_shoulder, right_elbow)


def angleLeftBodyLeg(landmarks):
    """
    Calculate the angle between the left knee, hip and shoulder.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    float: The angle in degrees between the left knee, hip and shoulder.

    Note:
    This function assumes that the input landmarks are in a 2D space.
    """
    left_knee = [
        landmarks[mp_pose.PoseLandmark.LEFT_KNEE.value].x,
        landmarks[mp_pose.PoseLandmark.LEFT_KNEE.value].y,
    ]
    left_hip = [
        landmarks[mp_pose.PoseLandmark.LEFT_HIP.value].x,
        landmarks[mp_pose.PoseLandmark.LEFT_HIP.value].y,
    ]
    left_shoulder = [
        landmarks[mp_pose.PoseLandmark.LEFT_SHOULDER.value].x,
        landmarks[mp_pose.PoseLandmark.LEFT_SHOULDER.value].y,
    ]

    return calculate_angle(left_knee, left_hip, left_shoulder)


def angleRightBodyLeg(landmarks):
    """
    Calculate the angle between the right knee, hip and shoulder.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    float: The angle in degrees between the right knee, hip and shoulder.

    Note:
    This function assumes that the input landmarks are in a 2D space.
    """
    right_knee = [
        landmarks[mp_pose.PoseLandmark.RIGHT_KNEE.value].x,
        landmarks[mp_pose.PoseLandmark.RIGHT_KNEE.value].y,
    ]
    right_hip = [
        landmarks[mp_pose.PoseLandmark.RIGHT_HIP.value].x,
        landmarks[mp_pose.PoseLandmark.RIGHT_HIP.value].y,
    ]
    right_shoulder = [
        landmarks[mp_pose.PoseLandmark.RIGHT_SHOULDER.value].x,
        landmarks[mp_pose.PoseLandmark.RIGHT_SHOULDER.value].y,
    ]

    return calculate_angle(right_knee, right_hip, right_shoulder)


def angleLeftLeg(landmarks):
    """
    Calculate the angle between the left ankle, knee, and hip.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    float: The angle in degrees between the left ankle, knee, and hip.

    Note:
    This function assumes that the input landmarks are in a 2D space.
    """
    left_ankle = [
        landmarks[mp_pose.PoseLandmark.LEFT_ANKLE.value].x,
        landmarks[mp_pose.PoseLandmark.LEFT_ANKLE.value].y,
    ]
    left_knee = [
        landmarks[mp_pose.PoseLandmark.LEFT_KNEE.value].x,
        landmarks[mp_pose.PoseLandmark.LEFT_KNEE.value].y,
    ]
    left_hip = [
        landmarks[mp_pose.PoseLandmark.LEFT_HIP.value].x,
        landmarks[mp_pose.PoseLandmark.LEFT_HIP.value].y,
    ]

    return calculate_angle(left_ankle, left_knee, left_hip)


def angleRightLeg(landmarks):
    """
    Calculate the angle between the right ankle, knee, and hip.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    float: The angle in degrees between the right ankle, knee, and hip.

    Note:
    This function assumes that the input landmarks are in a 2D space.
    """
    right_ankle = [
        landmarks[mp_pose.PoseLandmark.RIGHT_ANKLE.value].x,
        landmarks[mp_pose.PoseLandmark.RIGHT_ANKLE.value].y,
    ]
    right_knee = [
        landmarks[mp_pose.PoseLandmark.RIGHT_KNEE.value].x,
        landmarks[mp_pose.PoseLandmark.RIGHT_KNEE.value].y,
    ]
    right_hip = [
        landmarks[mp_pose.PoseLandmark.RIGHT_HIP.value].x,
        landmarks[mp_pose.PoseLandmark.RIGHT_HIP.value].y,
    ]

    return calculate_angle(right_ankle, right_knee, right_hip)


def arm_left_horizontal(landmarks):
    """
    Determine if the left arm is in a horizontal position.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    bool: True if the left arm is in a horizontal position, False otherwise.

    The function calculates the angles between the left shoulder, elbow, and wrist,
    and between the left hip, shoulder, and elbow. It checks if the angles fall within
    a specific range (160 to 200 degrees for the left arm and 60 to 110 degrees for the
    left body arm) to determine if the left arm is in a horizontal position.
    """
    angle_left_arm = angleLeftArm(landmarks)
    angle_left_body_arm = angleLeftBodyArm(landmarks)

    if (
        angle_left_arm > 160
        and angle_left_arm < 200
        and angle_left_body_arm > 60
        and angle_left_body_arm < 110
    ):
        return True
    else:
        return False


def arm_right_horizontal(landmarks):
    """
    Determine if the right arm is in a horizontal position.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    bool: True if the right arm is in a horizontal position, False otherwise.

    The function calculates the angles between the right shoulder, elbow, and wrist,
    and between the right hip, shoulder, and elbow. It checks if the angles fall within
    a specific range (160 to 200 degrees for the right arm and 60 to 110 degrees for the
    right body arm) to determine if the right arm is in a horizontal position.
    """
    angle_right_arm = angleRightArm(landmarks)
    angle_right_body_arm = angleRightBodyArm(landmarks)

    if (
        angle_right_arm > 160
        and angle_right_arm < 200
        and angle_right_body_arm > 60
        and angle_right_body_arm < 110
    ):
        return True
    else:
        return False


def arm_left_vertical(landmarks):
    """
    Determine if the left arm is in a vertical position.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    bool: True if the left arm is in a vertical position, False otherwise.

    The function calculates the angles between the left shoulder, elbow, and wrist,
    and between the left hip, shoulder, and elbow. It checks if the angles fall within
    a specific range (140 to 200 degrees for the left arm and 140 to 200 degrees for the
    left body arm) to determine if the left arm is in a vertical position.
    """
    angle_left_arm = angleLeftArm(landmarks)
    angle_left_body_arm = angleLeftBodyArm(landmarks)

    if (
        angle_left_arm > 140
        and angle_left_arm < 200
        and angle_left_body_arm > 140
        and angle_left_body_arm < 200
    ):
        return True
    else:
        return False


def arm_right_vertical(landmarks):
    """
    Determine if the right arm is in a vertical position.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    bool: True if the right arm is in a vertical position, False otherwise.

    The function calculates the angles between the right shoulder, elbow, and wrist,
    and between the right hip, shoulder, and elbow. It checks if the angles fall within
    a specific range (140 to 200 degrees for the right arm and 140 to 200 degrees for
    the right body arm) to determine if the right arm is in a vertical position.
    """
    angle_right_arm = angleRightArm(landmarks)
    angle_right_body_arm = angleRightBodyArm(landmarks)

    if (
        angle_right_arm > 140
        and angle_right_arm < 200
        and angle_right_body_arm > 140
        and angle_right_body_arm < 200
    ):
        return True
    else:
        return False


def arm_left_angle(landmarks):
    """
    Determine if the left arm is in a right-angled position.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    bool: True if the left arm is in a right-angled position (60 to 110 degrees for
    the left arm and left body arm), False otherwise.

    The function calculates the angles between the left shoulder, elbow, and wrist,
    and between the left hip, shoulder, and elbow. It checks if the angles fall within
    a specific range (60 to 110 degrees for both angles) to determine if the left arm is
    in a right-angled position.
    """
    angle_left_arm = angleLeftArm(landmarks)
    angle_left_body_arm = angleLeftBodyArm(landmarks)

    if (
        angle_left_arm > 60
        and angle_left_arm < 110
        and angle_left_body_arm > 60
        and angle_left_body_arm < 110
    ):
        return True
    else:
        return False


def arm_right_angle(landmarks):
    """
    Determine if the right arm is in a right-angled position.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    bool: True if the right arm is in a right-angled position (60 to 110 degrees for
    the right arm and right body arm), False otherwise.

    The function calculates the angles between the right shoulder, elbow, and wrist,
    and between the right hip, shoulder, and elbow. It checks if the angles fall within
    a specific range (60 to 110 degrees for both angles) to determine if the right arm is
    in a right-angled position.
    """
    angle_right_arm = angleRightArm(landmarks)
    angle_right_body_arm = angleRightBodyArm(landmarks)

    if (
        angle_right_arm > 60
        and angle_right_arm < 110
        and angle_right_body_arm > 60
        and angle_right_body_arm < 110
    ):
        return True
    else:
        return False


def leg_left_vertical(landmarks):
    """
    Determine if the left leg is in a vertical position.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    bool: True if the left leg is in a vertical position, False otherwise.

    The function calculates the angles between the left ankle, knee, and hip,
    and between the left hip, knee, and shoulder. It checks if the angles fall within
    a specific range (160 to 200 degrees for both angles) to determine if the left leg
    is in a vertical position.
    """
    angle_left_leg = angleLeftLeg(landmarks)
    angle_left_body_leg = angleLeftBodyLeg(landmarks)

    if (
        angle_left_leg > 160
        and angle_left_leg < 200
        and angle_left_body_leg > 160
        and angle_left_body_leg < 200
    ):
        return True
    else:
        return False


def leg_right_vertical(landmarks):
    """
    Determine if the right leg is in a vertical position.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    bool: True if the right leg is in a vertical position, False otherwise.

    The function calculates the angles between the right ankle, knee, and hip,
    and between the right hip, knee, and shoulder. It checks if the angles fall within
    a specific range (160 to 200 degrees for both angles) to determine if the right leg
    is in a vertical position.
    """
    angle_right_leg = angleRightLeg(landmarks)
    angle_right_body_leg = angleRightBodyLeg(landmarks)

    if (
        angle_right_leg > 160
        and angle_right_leg < 200
        and angle_right_body_leg > 160
        and angle_right_body_leg < 200
    ):
        return True
    else:
        return False


def leg_left_angle(landmarks):
    """
    Determine if the left leg is in a right-angled position.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    bool: True if the left leg is in a right-angled position (60 to 130 degrees for
    the left leg and left body leg), False otherwise.

    The function calculates the angles between the left ankle, knee, and hip,
    and between the left hip, knee, and shoulder. It checks if the angles fall within
    a specific range (60 to 130 degrees for both angles) to determine if the left leg is
    in a right-angled position.
    """
    angle_left_leg = angleLeftLeg(landmarks)
    angle_left_body_leg = angleLeftBodyLeg(landmarks)

    if (
        angle_left_leg > 60
        and angle_left_leg < 130
        and angle_left_body_leg > 60
        and angle_left_body_leg < 130
    ):
        return True
    else:
        return False


def leg_right_angle(landmarks):
    """
    Determine if the right leg is in a right-angled position.

    Parameters:
    landmarks (list): A list of 2D coordinates representing the keypoints of the body.

    Returns:
    bool: True if the right leg is in a right-angled position (60 to 130 degrees for
    the right leg and right body leg), False otherwise.

    The function calculates the angles between the right ankle, knee, and hip,
    and between the right hip, knee, and shoulder. It checks if the angles fall within
    a specific range (60 to 130 degrees for both angles) to determine if the right leg is
    in a right-angled position.
    """
    angle_right_leg = angleRightLeg(landmarks)
    angle_right_body_leg = angleRightBodyLeg(landmarks)

    if (
        angle_right_leg > 60
        and angle_right_leg < 130
        and angle_right_body_leg > 60
        and angle_right_body_leg < 130
    ):
        return True
    else:
        return False


def isPosition1(landmarks):
    """Define 1rst position
    
         O
       --|--
        / \
    """
    return (
        arm_left_horizontal(landmarks)
        and arm_right_horizontal(landmarks)
        and leg_left_vertical(landmarks)
        and leg_right_vertical(landmarks)
    )


def isPosition2(landmarks):
    """Define 2nd position
    
         O
       └-|--
        / \
    """
    return (
        arm_left_horizontal(landmarks)
        and arm_right_angle(landmarks)
        and leg_left_vertical(landmarks)
        and leg_right_vertical(landmarks)
    )


def isPosition3(landmarks):
    """Define 3rd position
    
         O
       └-|-┘
        / \
    """
    return (
        arm_left_angle(landmarks)
        and arm_right_angle(landmarks)
        and leg_left_vertical(landmarks)
        and leg_right_vertical(landmarks)
    )


def isPosition4(landmarks):
    """Define 4th position
        
         O
       --|-┘
        / \
    """
    return (
        arm_left_angle(landmarks)
        and arm_right_horizontal(landmarks)
        and leg_left_vertical(landmarks)
        and leg_right_vertical(landmarks)
    )


def isPosition5(landmarks):
    """Define 5th position
        
        \O/
         |
        / \
    """
    return (
        arm_left_vertical(landmarks)
        and arm_right_vertical(landmarks)
        and leg_left_vertical(landmarks)
        and leg_right_vertical(landmarks)
    )


def isPosition6(landmarks):
    """Define 6th position
        
        \O__
         |
        / \
    """
    return (
        arm_left_horizontal(landmarks)
        and arm_right_vertical(landmarks)
        and leg_left_vertical(landmarks)
        and leg_right_vertical(landmarks)
    )


def isPosition7(landmarks):
    """Define 7th position
        
       __O/
         |
        / \
    """
    return (
        arm_left_vertical(landmarks)
        and arm_right_horizontal(landmarks)
        and leg_left_vertical(landmarks)
        and leg_right_vertical(landmarks)
    )


def isPosition8(landmarks):
    """Define 8th position

    __O__
     _|_
    |   |
    """
    return (
        arm_left_horizontal(landmarks)
        and arm_right_horizontal(landmarks)
        and leg_left_angle(landmarks)
        and leg_right_angle(landmarks)
    )


def isPosition9(landmarks):
    """Define 9th position

      O
    └-|-┘
     _|_
    |   |
    """
    return (
        arm_left_angle(landmarks)
        and arm_right_angle(landmarks)
        and leg_left_angle(landmarks)
        and leg_right_angle(landmarks)
    )
