import os
import tkinter as tk

from PIL import Image, ImageTk


class ImageSelectionApp:
    """
    A class to create a GUI application for image selection.

    Attributes
    ----------
    root : tk.Tk
        The root window of the application.
    selected_images : set
        A set to store the selected image file paths.
    image_labels : list
        A list to store the image labels and their corresponding buttons.
    current_index : int
        The index of the current image being displayed.

    Methods
    -------
    __init__(self, root)
        Initializes the application with the root window.
    create_widgets(self)
        Creates the widgets for the application.
    browse_images(self)
        Browses and displays the images in the specified directory.
    toggle_selection(self, label)
        Toggles the selection of an image when its button is clicked.
    save_selection(self)
        Saves the selected image file paths to a text file.
    next_image(self)
        Displays the next image in the directory.
    """

    def __init__(self, root):
        """
        Initializes the application with the root window.

        Parameters
        ----------
        root : tk.Tk
            The root window of the application.
        """
        self.root = root
        self.root.title("Sélectionner les positions")

        self.selected_images = set()
        self.image_labels = []
        self.current_index = 0

        self.create_widgets()

    def create_widgets(self):
        """
        Creates the widgets for the application.

        This method creates the main label, frame, and next button. It also calls the
        `browse_images` method to display the images in the specified directory.

        Parameters:
        None

        Returns:
        None
        """
        self.label = tk.Label(
            self.root, text="Sélectionner les positions", font=("Arial", 24)
        )
        self.label.pack()

        self.frame = tk.Frame(self.root)
        self.frame.pack()

        self.browse_images()

        next_button = tk.Button(
            self.frame,
            text="Suivant",
            command=self.next_image,
            activebackground="#738082",
            font=("Arial", 12),
            fg="#eeeeee",
            bg="#353B3C",
        )
        next_button.pack(side=tk.RIGHT)
        self.next_button = next_button

    def browse_images(self):
        """
        Browses and displays the images in the specified directory.

        This method iterates through the files in the "./images/" directory, filters
        the files starting with "pose", and stores their file paths in the `filepaths`
        list.
        It then destroys the existing image labels and buttons, clears the
        `image_labels` list, and iterates through the sorted `filepaths` list. If the
        index matches the `current_index`, it opens the image file, creates a
        `PhotoImage` object, creates a `Label` widget with the image, and creates a
        `Button` widget for selecting or deselecting the image. The `Label` and `Button`
        widgets are then appended to the `image_labels` list and packed into the GUI.

        Parameters:
        None

        Returns:
        None
        """
        filepaths = []
        for file in os.listdir("./images/"):
            if file.startswith("pose"):
                filepaths.append("./images/" + file)

        for label, select_button in self.image_labels:
            label.destroy()
            select_button.destroy()
        self.image_labels.clear()

        for i, filepath in enumerate(sorted(filepaths)):
            if i == self.current_index:
                image = Image.open(filepath)
                photo = ImageTk.PhotoImage(image)

                label = tk.Label(self.root, image=photo, bd=3)
                label.photo = photo
                label.filepath = filepath

                button_text = (
                    "Sélectionner"
                    if label.filepath not in self.selected_images
                    else "Désélectionner"
                )

                select_button = tk.Button(
                    self.frame,
                    text=button_text,
                    command=lambda L=label: self.toggle_selection(L),
                    activebackground="#D7C2FF",
                    font=("Arial", 12),
                    fg="#010101",
                    bg="#B892FF",
                )
                select_button.pack(side=tk.LEFT)

                self.image_labels.append((label, select_button))
                label.pack()

                break

    def toggle_selection(self, label):
        """
        Toggles the selection of an image when its button is clicked.

        Parameters:
        label (tk.Label): The label widget that displays the image.

        Returns:
        None
        """
        if label.filepath in self.selected_images:
            # If the image is already selected, remove it from the selection set
            # and change the background color of the label to white
            self.selected_images.remove(label.filepath)
            label.config(bg="white")
        else:
            # If the image is not selected, add it to the selection set
            # and change the background color of the label to red
            self.selected_images.add(label.filepath)
            label.config(bg="#e94e4e")

        # Update the text of the button to reflect the current selection status
        button_text = (
            "Sélectionner"
            if label.filepath not in self.selected_images
            else "Désélectionner"
        )

        # Iterate through the image labels and buttons to find the corresponding button
        # for the clicked label and update its text
        for lbl, btn in self.image_labels:
            if lbl == label:
                btn.config(text=button_text)

    def save_selection(self):
        """
        Saves the selected image file paths to a text file.

        This method opens a file named "selected_positions.txt" in write mode. It then
        iterates through the sorted `selected_images` set, writes each file path to the
        file, and appends a newline character. After writing all the file paths, it
        closes the file.
        Finally, it destroys the root window to close the application.

        Parameters:
        None

        Returns:
        None
        """
        with open("selected_positions.txt", "w") as file:
            for filepath in sorted(self.selected_images):
                file.write(filepath + "\n")

        self.root.destroy()

    def next_image(self):
        """
        Displays the next image in the directory and updates the GUI.

        If the current image is the last one in the directory, it destroys the "Next"
        button and creates a "Finish" button to save the selected images and close the
        application.

        Parameters:
        None

        Returns:
        None
        """
        self.current_index += 1  # Increment the current index to display the next image

        filepaths = []
        for file in os.listdir("./images/"):
            if file.startswith("pose"):
                filepaths.append("./images/" + file)  # Collect all image file paths

        # If the current index is the last image, destroy the "Next" button
        # and create a "Finish" button
        if self.current_index >= len(filepaths) - 1:
            self.next_button.destroy()

            finish_button = tk.Button(
                self.frame,
                text="Terminer",
                command=self.save_selection,
                activebackground="#F29292",
                font=("Arial", 12),
                fg="#eeeeee",
                bg="#e94e4e",
            )
            finish_button.pack(side=tk.RIGHT)

        # Display the next image
        self.browse_images()


if __name__ == "__main__":
    """
    Entry point of the application.

    Initializes the root window, creates an instance of the ImageSelectionApp,
    and starts the Tkinter event loop.

    Parameters
    ----------
    None

    Returns
    -------
    None
    """
    root = tk.Tk()  # Initialize the root window
    app = ImageSelectionApp(root)  # Create an instance of the ImageSelectionApp

    root.mainloop()  # Start the Tkinter event loop
